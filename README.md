[#](#) wiki.social.coop

## Overview

This repository contains a NodeJS server for serving up a (mostly) static site using Metalsmith. Prior to social.coop adopting Mediawiki at wiki.social.coop this repository would build Markdown pages that were stored in the https://git.coop/social.coop/general repository. After moving to Mediawiki this application was left running only to respond to registration requests for Mastodon on Meet.Coop.

## Content

- wiki is a git repo under the hood (it gets cloned into src/)
- All commits to [social.coop/general wiki] trigger a webhook call to the wiki.social.coop server, which then performs a `git pull` to fetch the latest content.
- wiki also holds some templates (like gitlab issue and email) **which aren't
  visible through the gitlab wiki interface**, so you need to clone the repo to
  edit them

## Authenticated routes

- to secure a page behind mastodon authentication you need a handler using
  `ensureAuthenticated` and (if you want to load a page from the wiki after
  authentication) calling `next()`
- ensureAuthenticated basically redirects to social.coop through [the mastodon
  oauth flow](https://docs.joinmastodon.org/spec/oauth/) requesting the minimum
  permissions (read:accounts) and, if successful, saves the user's mastodon
  username into a signed cookie (to avoid redirecting next time)

## Deployment

- the wiki is deployed by [our ansible
  config](https://git.coop/social.coop/tech/ansible) like so: `ansible-playbook
  server.playbook.yml --tags wiki --user $YOUR_SERVER_USERNAME`
- the code grabs various values from a `config.js` file (see
  [example-config.js](https://git.coop/social.coop/tech/wiki.social.coop/-/blob/master/example-config.js))
  which exists in the ansible config as
  [config.js.j2](https://git.coop/social.coop/tech/ansible/-/blob/master/roles/wiki.social.coop/templates/config.js.j2)
- the secret values in config.js are templated in config.js.j2 and mapped in
  [server.playbook.yaml](https://git.coop/social.coop/tech/ansible/-/blob/master/server.playbook.yml)
  to secrets encypted in [our pass repo](https://git.coop/social.coop/tech/pass)
  
## Development

```
$ git clone git@git.coop:social.coop/tech/wiki.social.coop.git
$ cd wiki.social.coop
$ git clone git@git.coop:social.coop/general.wiki src
$ cp example-config.json config.json
$ npm run serve
```

If you want to change the wiki source pages in `src` you will need to run `npm run build` afterwards to tell metalsmith to rebuild the static site.

[social.coop/general wiki]: https://git.coop/social.coop/general/-/wikis/home
