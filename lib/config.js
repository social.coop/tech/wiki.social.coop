'use strict';
const currentSchema = 2; // Defines the schema version we expect.
const fs = require('fs');
const configFile = `${__dirname}/../config.js`;
const config = {
    ...require('./default-config.js'),
    ...(fs.existsSync(configFile)? require(configFile) : {}),
};

if (!config.schema || Number(config.schema) <= 0) {
    throw new Error([
        "Your config file has no valid schema number. This means it",
        "is probably an old version, and in any case we cannot",
        "upgrade it automatically. You must upgrade it manually.",
        "See the documentation in the source: doc/config/Upgrading.md.",
    ].join(" "))
}

if (config.schema !== currentSchema) {
    throw new Error([
        `Your config file is the wrong schema for the code: ${config.schema}.`,
        `This code requires version ${currentSchema}.`,
        "Currently you must upgrade/downgrade it manually.",
        "See the documentation in the source: doc/config/Upgrading.md.",
    ].join(" "))
}

module.exports = config;
