const express = require('express');
const router = express.Router();
const axios = require('axios');
const url = require('url');

let config = require('./config.js');
const baseUrl = config.theme.urls.base;
config = config.oauth;

// TODO: logging, potentially remove false-positive error logs below

const ensureAuthenticated = async (req, res, next) => {
    const redirectUri = baseUrl + "/auth/login" + req.url;

    const authenticated = !!req.signedCookies.socialCoopUser

    if (!authenticated) {
        try {
            return res.redirect(
                `${config.instance}/oauth/authorize?client_id=${config.clientId}&scope=read:accounts&redirect_uri=${redirectUri}&response_type=code`
            );
        } catch (err) {
            console.log('error redirecting to login:', err);
            return res.json("an error occurred: check wiki config.js contains oauth values")
        }
    }

    next();
};

router.get("/login/:destination", async (req, res) => {
    const [path, code] = req.url.split('?code=');
    const redirectUri = baseUrl + "/auth" + path
    const destinationPath = "/" + req.params.destination

    const params = new url.URLSearchParams({
        client_id: config.clientId,
        client_secret: config.clientSecret,
        redirect_uri: redirectUri,
        grant_type: 'authorization_code',
        code: code,
        scope: 'read:accounts',
    });

    let token;

    try {
        const tokenResponse = await axios.post(
            `${config.instance}/oauth/token`,
            params.toString()
        );
        token = tokenResponse.data.access_token;
    } catch (err) {
        console.log('error requesting access token:', err.response.data || err);
    }

    try {
        const profileResponse = await axios.get(
            `${config.instance}/api/v1/accounts/verify_credentials`,
            {
                headers: { Authorization: 'Bearer ' + token },
            }
        );
        if (profileResponse.status === 200) {
            const { username } = profileResponse.data
            console.log(
                'successfully logged in',
                username
            );
            res.cookie("socialCoopUser", username, { signed: true })
            console.log("redirecting to", destinationPath)
            res.redirect(destinationPath)
        } else {
            throw new Error(
                `verify_credentials response was ${profileResponse.status}, body: ${profileResponse.data}`
            );
        }
    } catch (err) {
        console.log('error verifying credentials:', err.response.data || err);
    }
})

module.exports = { router, ensureAuthenticated };
