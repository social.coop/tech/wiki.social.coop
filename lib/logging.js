'use strict';
const winston = require('winston');
const { format, transports } = winston;
const { combine, timestamp, metadata, json, label, errors } = format;
const config = require('./config.js').logging;

const categories = ['build', 'http', 'hook', 'submit', 'mail'];
function mkConfig(category, overrides = {}) {
    return {
        format: combine(
            timestamp({format: 'YYYY-MM-DD hh:mm:ss'}),
            metadata(),
            json(),
            label({label: category}),
            errors({stack: true})
        ),
        transports: [
            new transports.Console({level: 'info'}),
        ],
        ...overrides,
    };
}

const loggers = {};

// Create loggers for each category, store in winston.loggers
categories.forEach((category) => {
    var categoryConfig;
    switch(typeof(config[category])) {
    case 'function':
        categoryConfig = config[category](category, config.default);
        break;
    case 'object':
        categoryConfig = mkConfig(category, config[category]);
        break;
    case 'undefined':
        categoryConfig = mkConfig(category, config.default);
        break;
    default:
        throw new Error("logger config clauses must be functions or objects, not "+typeof(config[category]));
    }
    winston.loggers.add(category, categoryConfig);
    loggers[category] = winston.loggers.get(category);
});

module.exports = {
    loggers, mkConfig
};
