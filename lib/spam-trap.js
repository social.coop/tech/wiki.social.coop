'use strict';
/* This module implements honeytrap form fields 
 *
 * These try and spot and drop spammy responses to form.
 */

const submitlog = require('./logging.js').loggers.submit;

// Constructs a field value checker
function makeFieldChecker(expected) {
    return function(fields) {
        for(var name in expected) {
            if (expected[name] !== fields[name]) {
                this.log.info(
                    `honeypot field ${name} tripped with '${fields[name]}' != '${expected[name]}'.`,
                    fields
                );
                return true;
            }
            delete fields[name]; // This field is a decoy
        }
        return false;
    };
}

module.exports = (config) => {
    // If no config, no validation.
    if (!config) {
        submitlog.info("No spam traps configured");
        return () => false;
    }

    submitlog.info(`Configured ${config.length} spamtraps`);

    // Config is an array of items.  Each item can be a function, or a
    // name/value map.  The latter just define expected values for a
    // non-spam response. The former takes a map of field names to
    // values, and returns true if it is spam.
    const checks = [];
    for(var ix = 0; ix < config.length; ix += 1) {
        var check = config[ix];
        if (typeof(check) !== 'function') {
            check = makeFieldChecker(check);
        }

        checks.push(check);
    }
    
    return (fields) => {
        const that = {log: submitlog};
        for(var ix = 0; ix < checks.length; ix += 1) {
            if (!checks[ix].call(that, fields))
                continue;
            submitlog.info(`spammy response, dropping`, fields);
            return true;
        }
        return false;
    };
}
