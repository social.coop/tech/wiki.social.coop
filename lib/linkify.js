module.exports = function (rawUrl, prefix) {
    const url = rawUrl.trim()
    if (!url.startsWith('http')) {
        if (url.startsWith(prefix)) {
            return 'https://' + url;
        } else {
            if (url.startsWith('/') && prefix.endsWith('/')) {
                return 'https://' + prefix + url.split('/')[1];
            }
            if (!url.startsWith('/') && !prefix.endsWith('/')) {
                return 'https://' + prefix + '/' + url;
            }
            return 'https://' + prefix + url;
        }
    }

    return url;
};
