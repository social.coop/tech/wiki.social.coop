'use strict';
const email = require('emailjs');
const toText = require('html2plaintext');

module.exports = (serverConfig = {}) => {

    /** Sends a notification email.  
     *
     * @param config - the emailjs config
     * @param html - the HTML message (will have a text version added)
     * @param json - optional JSON attachment (the registration data)
     * @returns a promise which can be used to obtain the success
     * or failure of this (potentially slow) operation.
     */
    async function send(config, html, json) {
        if (!config.from)
            throw new Error("must send with a from: parameter");
        if (!config.to)
            throw new Error("must send with a to: parameter");
        
        var html = String(html);
        const server = email.server.connect(serverConfig);
        const attachments = [{data: html, alternative: true}];
        if (json)
            attachments.push({
                data: JSON.stringify(json),
                name: 'form-data.json',
                type: 'application/json'
            });
        const message = {
            ...config,
            text: toText(html),
            attachment: attachments
        };
        
        // Send the message and await completion. Return
        // the details of the sent message.
        return await new Promise((resolve, reject) => {
            server.send(message, (err, message) => {
                if (err)
                    reject(err);
                else
                    resolve(message);
            });
        });
    }

    return send;
};
