const test = require('ava');
const linkify = require('../linkify.js');

test("doesn't mangle good urls", (t) => {
    const httpsUrl = 'https://opencollective.com/example';
    const httpUrl = '  http://opencollective.com/example';
    t.is(linkify(httpsUrl, 'opencollective.com'), httpsUrl);
    t.is(linkify(httpUrl, 'opencollective.com'), httpUrl.trim());
});

test('linkifies urls without http', (t) => {
    const url = 'social.coop/foobar  ';
    t.is(linkify(url, 'social.coop'), 'https://social.coop/foobar');
});

test('linkifies urls without prefix', (t) => {
    const url = 'foobar';
    t.is(linkify(url, 'social.coop'), 'https://social.coop/foobar');
});

test('does the right thing about slash before the prefix', (t) => {
    const withSlash = '  /foobar ';
    const withoutSlash = 'foobar';
    desiredUrl = 'https://social.coop/foobar';
    t.is(linkify(withSlash, 'social.coop'), desiredUrl);
    t.is(linkify(withoutSlash, 'social.coop'), desiredUrl);
    t.is(linkify(withSlash, 'social.coop/'), desiredUrl);
    t.is(linkify(withoutSlash, 'social.coop/'), desiredUrl);
});
