'use strict';
const express = require('express');
const cookieParser = require('cookie-parser');
const fs = require('fs');
const { router: authRouter, ensureAuthenticated } = require('./lib/auth.js')
const templates = require('./lib/templates.js');
const metalsmith = require('./metalsmith.js');
const email = require('./lib/email.js');
const gitlab = require('./lib/gitlab.js');
const git = require('./lib/git.js')({});
const loggers = require('./lib/logging.js').loggers;
const config = require('./lib/config.js');
const spamTrap = require('./lib/spam-trap.js')(config.spamTraps);
const linkify = require('./lib/linkify.js');

const buildlog = loggers.build;
const httplog = loggers.http;
const hooklog = loggers.hook;
const submitlog = loggers.submit;
const maillog = loggers.mail;

const send = email(config.email.server);
const app = express();
const port = config.port;

var expressOptions = {
    dotfiles: 'ignore',
    etag: false,
    //  extensions: ['htm', 'html'],
    index: false,
    maxAge: '1d',
    redirect: false,
    setHeaders: function (res, path, stat) {
        res.set('x-timestamp', Date.now());
    }
};

var fileOptions = {
    root: __dirname + '/build/',
    dotfiles: 'deny',
    headers: {
        'x-timestamp': Date.now(),
        'x-sent': true,
    }
};

/* 
 * All wiki content lives in mediawiki at wiki.social.coop, so we need to make
 * sure all traffic for the old pages is redirected there.
 *
 * TODO: this app should have all wiki content gutted from it and should only be 
 * used for Mastodon and Meet.Coop registrations.
 */
var redirects = {
  '/Governance': 'https://wiki.social.coop/wiki/Governance',
  '/Operations': 'https://wiki.social.coop/wiki/Operations',
  '/Platforms': 'https://wiki.social.coop/wiki/Platforms',
  '/Tutorials': 'https://wiki.social.coop/wiki/Tutorials',
  '/How-to-make-the-fediverse-your-own': 'https://wiki.social.coop/wiki/How_to_Make_the_Fediverse_Your_Own',
  '/Social.coop-guide-to-Mastodon': 'https://wiki.social.coop/wiki/Social.coop_guide_to_Mastodon',
  '/Make-a-proposal': 'https://wiki.social.coop/wiki/Make_a_proposal',
  '/May-First': 'https://wiki.social.coop/wiki/May_First_Movement_Technology',
  '/community-working-group/Community-Working-Group-Ops-Team': 'https://wiki.social.coop/wiki/Community_Working_Group_Ops_Team',
  '/community-working-group/Community-Working-Group': 'https://wiki.social.coop/wiki/Community_Working_Group',
  '/community-working-group/Conflict-resolution-guide': 'https://wiki.social.coop/wiki/Conflict_resolution_guide',
  '/community-working-group/Guide-for-CWG-Ops-Team-"On-Call"': 'https://wiki.social.coop/wiki/Guide-for-CWG-Ops-Team-%22On-Call%22',
  '/community-working-group/Reporting-guide': 'https://wiki.social.coop/wiki/Reporting_guide',
  '/Getting-a-social.coop-GitLab-project-account': 'https://wiki.social.coop/wiki/Getting_a_GitLab_project_account',
  '/docs/Bylaws': 'https://wiki.social.coop/wiki/Bylaws',
  '/docs/Code-of-conduct': 'https://wiki.social.coop/wiki/Code_of_conduct',
  '/docs/Defederation-of-instances': 'https://wiki.social.coop/wiki/Defederation_of_instances',
  '/docs/Federation-Abuse-Policy': 'https://wiki.social.coop/wiki/Federation_abuse_policy',
  '/docs/Partnership-with-Platform-6': 'https://wiki.social.coop/wiki/Partnership_with_Platform_6',
  '/finance-working-group/Finance-Working-Group': 'https://wiki.social.coop/wiki/Finance_Working_Group',
  '/legal-working-group/Legal-Working-Group': 'https://wiki.social.coop/wiki/Legal_Working_Group',
  '/platforms/Calendars': 'https://wiki.social.coop/wiki/Calendars'
}

async function build() {
    buildlog.info('starting build...')
    metalsmith().build(function(err) {
        if (err)
            buildlog.error('build exception', err);
    })
    buildlog.info('completed build.')
}

app.set('trust proxy', true); // Assume we're behind a proxy
app.use(express.static('/', expressOptions));
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cookieParser(config.cookieSecret, {
    maxAge: 60 * 60 * 24 * 30, // one month
    httpOnly: true,
    secure: true,
}))

app.use('/auth', authRouter)

app.all('*', (req, res, next) => {
    next();
    httplog.info(`${req.method} ${req.url} ${req.hostname} ${req.ip} ${res.statusCode}`, {
        method: req.method,
        url: req.url,
        ip: req.ip,
        host: req.hostname,
        code: res.statusCode,
    });
})
app.get('/', (req, res) => {
    res.redirect('/home.html');
});
app.get('/style.css', (req, res) => {
    res.sendFile(req.path, fileOptions);
});
app.get(/^\/meet.coop-registration-form(.html)?/, ensureAuthenticated, (req, res, next) => {
    next();
});
app.get('*', (req, res, next) => {

    // redirect to the new wiki using both bare and .html URLs
    let redirectPath = req.path.replace(/\.html$/, '');
    if (redirectPath in redirects) {
      res.redirect(301, redirects[redirectPath]);
      return;
    }

    var path = req.path+'/index.html';
    if (fs.existsSync(fileOptions.root+path)) {
        // Cater to the wiki's nested-indexes style
        res.sendFile(path, fileOptions);
        return;
    }
    if (fs.existsSync(fileOptions.root+req.path)) {
        res.sendFile(req.path, fileOptions);
        return;
    }
    path = req.path+'.html';
    if (fs.existsSync(fileOptions.root+path)) {
        res.redirect(path);
        return;
    }
    next();
});

if (!config.webhook) {
    hooklog.warn("No webhook configured!");
}
else{
    app.post(config.webhook.path, (req, res) => {
        var secret = req.header('X-Gitlab-Token');
        if (secret !== config.webhook.secret) {
            res.sendStatus(403); // Unauthorised
            hooklog.warn(`rejecting unauthorised hook call`);
            return;
        }
        
        try {
            hooklog.info(`handling hook call by pulling`);
            git // Async
                .pull()
                .then(build)
                .then(() => hooklog.info(`handling hook call complete`));
        }
        catch(e) {
            hooklog.error('error handling hook', e);
        }
        res.sendStatus(200);
    });
}
app.post('/registration', (req, res) => {
    const sendConfig = config.email.registration || {};
    const templateContext = {
        service: "social.coop",
        config: {theme: config.theme},
        request: req,
    };
    
    function ok() {}
    function error() {
        return (err) => submitlog.info(`$message: `, err);
    }

    try {
      req.body['oc-user'] = linkify(req.body['oc-user'], 'opencollective.com');
    } catch(err) {
      submitlog.info("no OpenCollective entered for: " + JSON.stringify(req.body)); 
    }
    
    // This function logs spamtrap hits, we just need to drop spam.
    // Previously we sent a false ack email to spam requests, but
    // this was causing problems with Mailgun! So, no more false acks.
    if (!spamTrap(req.body)) {
        if (req.body.email) {
            // Send acknowledgement email to registrant
            const acknowledgementConfig = {
                from: sendConfig.from,
                to: req.body.email,
                subject: sendConfig.acknowledgement,
            };
            try {
                const html = templates.acknowledgement(templateContext);
                submitlog.info(`emailing registration acknowledgement to ${acknowledgementConfig.to}: `);
                send(acknowledgementConfig,
                     html)
                    .then(ok, error("error emailing registration acknowledgement"));
            }
            catch(e) {
                maillog.error(`failed to send registration acknowledgement to ${acknowledgementConfig.to} `, e);
            }
        }
        else {
            maillog.error(`cannot send registration acknowledgement, no email set in registration`);
        }

        if (gitlab) {
            try {
                gitlab.newRegistrationIssue(templateContext)
                    .then(ok, error("error posting new gitlab issue"));
            }
            catch(e) {
                submitlog.info('error posting new gitlab issue: ', e);
            }
        }

        // Send notification of registration to admins
        submitlog.info(`emailing registration notification to ${sendConfig.to}`, req.body);
        const notificationConfig = {
            from: sendConfig.from,
            to: sendConfig.to,
            subject: sendConfig.notification,
        };
        try {
            const html = templates.notification(templateContext);
            send(notificationConfig,
                 html,
                 req.body)
                .then(ok, error("error emailing registration notification"));
        }
        catch(e) {
            maillog.error(`failed to send notification of registration to ${notificationConfig.to}: `, e);
        }
    }
    res.redirect('/home');
});

app.post('/meet.coop-registration-form', (req, res) => {
    const sendConfig = config.email.meetCoopRegistration || {};
    const templateContext = {
        service: "meet.coop",
        config: {theme: config.theme},
        request: req,
    };

    function ok() {}
    function error() {
        return (err) => submitlog.info(`$message: `, err);
    }

    req.body['oc-user'] = linkify(req.body['oc-user'], 'opencollective.com')
    req.body['mastodon'] = linkify(req.body['mastodon'], 'social.coop')

    if (!spamTrap(req.body)) {
        console.log("ok")
        if (req.body.email) {
            // Send acknowledgement email to registrant
            const acknowledgementConfig = {
                from: sendConfig.from,
                to: req.body.email,
                subject: sendConfig.acknowledgement,
            };
            try {
                const html = templates.meetCoopAcknowledgement(templateContext);
                submitlog.info(`emailing meet.coop registration acknowledgement to ${acknowledgementConfig.to}: `);
                send(acknowledgementConfig,
                     html)
                    .then(ok, error("error emailing meet.coop registration acknowledgement"));
            }
            catch(e) {
                maillog.error(`failed to send meet.coop registration acknowledgement to ${acknowledgementConfig.to} `, e);
            }
        }
        else {
            maillog.error(`cannot send meet.coop registration acknowledgement, no email set in registration`);
        }

        if (gitlab) {
            try {
                gitlab.newRegistrationIssue(templateContext)
                    .then(ok, error("error posting new gitlab issue"));
            }
            catch(e) {
                submitlog.info('error posting new gitlab issue: ', e);
            }
        }

        // Send notification of registration to admins
        submitlog.info(`emailing meet.coop registration notification to ${sendConfig.to}`, req.body);
        const notificationConfig = {
            from: sendConfig.from,
            to: sendConfig.to,
            subject: sendConfig.notification,
        };
        try {
            const html = templates.meetCoopNotification(templateContext);
            send(notificationConfig,
                 html,
                 req.body)
                .then(ok, error("error emailing meet.coop registration notification"));
        }
        catch(e) {
            maillog.error(`failed to send notification of meet.coop registration to ${notificationConfig.to}: `, e);
        }
    }
    res.redirect('/Thank-you-for-registering-for-Meet.coop.html');
});

app.listen(port, () => console.log(`serve.js listening on port ${port}!`));
