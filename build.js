'use strict';
const metalsmith = require('./metalsmith.js');
const loggers = require('./lib/logging.js').loggers;

const buildlog = loggers.build;

buildlog.info('starting build...')
metalsmith().build(function(err) {
    if (err)
        buildlog.error('build exception', err);
})
buildlog.info('completed build.')